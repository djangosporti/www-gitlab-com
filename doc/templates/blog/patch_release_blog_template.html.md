---
title: "GitLab Patch Release: x.x.x and x.x.x"
categories: release
author: ADD_YOUR_FULL_NAME
author_gitlab: GITLAB-COM_USERNAME
author_twitter: TWITTER_USERNAME
description: "Short description of the blog post"
---

Today we are releasing version X.Y.Z for GitLab Community Edition and Enterprise Edition.

This version resolves a number of regressions and bugs in
[this month's x.x.w release](/YYYY/MM/DD/gitlab-x-dot-x-dot-w-released/) and
prior versions.

<!-- more -->

## GitLab Community Edition and Enterprise Edition

Available in GitLab Core, Starter, Premium, and Ultimate:

- [Description](CE MR LINK)
- [Description](CE MR LINK)

Available in GitLab Starter, Premium, and Ultimate:

- [Description](EE MR LINK)
- [Description](EE MR LINK)

Available in GitLab Premium and Ultimate:

- [Description](EE MR LINK)
- [Description](EE MR LINK)

Available in GitLab Ultimate:

- [Description](EE MR LINK)
- [Description](EE MR LINK)

## Upgrade barometer

This version does not include any new migrations, and should not require any
downtime.

Please be aware that by default the Omnibus packages will stop, run migrations,
and start again, no matter how “big” or “small” the upgrade is. This behavior
can be changed by adding a [`/etc/gitlab/skip-auto-migrations`](http://docs.gitlab.com/omnibus/update/README.html) file,
which is only used for [updates](https://docs.gitlab.com/omnibus/update/README.html).

## Updating

To update, check out our [update page](https://about.gitlab.com/update/).

## GitLab subscriptions

Access to GitLab Starter, Premium, and Ultimate features is granted by a paid [subscription](https://about.gitlab.com/products/).

Alternativelly, [sign up for GitLab.com](https://about.gitlab.com/gitlab-com/)
to use GitLab's own infrastructure.
