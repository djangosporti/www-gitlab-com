---
layout: markdown_page
title: "Webcasts"
---

## On this page
{:.no_toc}

- TOC
{:toc}  


## Upcoming Webcasts  

### GitLab for Enterprises
This is a reoccuring demo webcast that is held every two weeks on Wednesday morning. You'll get a first-hand look at how GitLab integrates version control, project management, code review, testing, and deployment to help enterprise teams move faster from planning to monitoring.   

The webcast starts at 8am PDT / 3pm UTC, you can [register](https://about.gitlab.com/eep-demo) for any of the upcoming dates: 
- April 11
- April 25
- May 9


### Overcoming Barriers to DevOps Automation - a 4-part series
**April 25th**: Let's Talk DevOps and Drawbacks   
**May 9th**: Solving the Collaboration Conundrum: How to Make the DevOps Dream a Reality    
**May 23rd**: Removing Barriers Between Dev and Ops: Setting Up a CI/CD Pipeline   
**June 6th**: Getting Started with CI/CD on GitLab   

Each webcast starts at 9am PDT / 4pm UTC, you can [register](https://about.gitlab.com/webcast/devops-automation) for one or the entire series. 


### Scalable app deployment with GitLab and Google Cloud Platform   
**April 26th** - 2 sessions: 7am PDT / 2pm UTC or 11am PDT / 6pm UTC   
[Register](https://about.gitlab.com/webcast/scalabel-app-deploy) for either session to learn how to unlock scalable app deployment in a few clicks with the GitLab GKE integration. 


